//
//  ViewController.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 30/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    var presenter: CharactersPresenter = CharactersPresenter()
    var characters: [Character] = []
    let cellReuseIdentifier: String = "cell"

    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.getCharacters { (characters) in
            self.characters = characters
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func registerTableView() {
        self.tableView.register(UINib(nibName: "CharacterTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func goToDetails(character: Character) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nav = self.navigationController, let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return
        }
        vc.character = character
        nav.pushViewController(vc, animated: true)
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let character = self.characters[indexPath.row]
        
        guard let cell: CharacterTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? CharacterTableViewCell else {
            return UITableViewCell()
        }
        
        cell.nameCharacter.text = character.name
        cell.descriptionCharacter.text = character.description
        if character.thumbnail.path != "" {
            let url = character.thumbnail.path + "." + character.thumbnail.extensionString
            cell.imgCharacter.imageFromServerURL(urlString: url)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let character = self.characters[indexPath.row]
        
        self.goToDetails(character: character)
    }
    
}
