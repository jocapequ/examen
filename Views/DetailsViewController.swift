//
//  DetailsViewController.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 30/06/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var presenter: DetailsPresenter = DetailsPresenter()

    @IBOutlet weak var imgDetails: UIImageView!
    @IBOutlet weak var nameDetails: UILabel!
    @IBOutlet weak var descriptionDetails: UILabel!
    
    public var character: Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let character = self.character else {
            return
        }
        
        let (name, description, url) = presenter.getValuesFromModel(character: character)
        nameDetails.text = name
        descriptionDetails.text = description
        imgDetails.imageFromServerURL(urlString: url)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
