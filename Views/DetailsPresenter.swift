//
//  DetailsPresenter.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 30/06/21.
//

import Foundation

public protocol DetailsPresenterProtocol {
    func getValuesFromModel(character: Character) -> (String, String, String)
}

class DetailsPresenter {
    
    public struct Dependencies {
        
    }
    
    init(dependencies: Dependencies = .init()) {
        
    }
}

extension DetailsPresenter: DetailsPresenterProtocol {
    func getValuesFromModel(character: Character) -> (String, String, String) {
        var url: String = ""
        if character.thumbnail.path != "" {
            url = character.thumbnail.path + "." + character.thumbnail.extensionString
        }
        
        return (character.name, character.description, url)
    }
}
