//
//  UIImage+Extension.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 30/06/21.
//

import Foundation
import UIKit

extension UIImageView {
  public func imageFromServerURL(urlString: String) {
    
    URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
      
      if error != nil {
        return
      }
      
      DispatchQueue.main.async(execute: { () -> Void in
        let image = UIImage(data: data!)
        self.image = image
      })
      
    }).resume()
  }
}
