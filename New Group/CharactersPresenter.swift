//
//  CharactersPresenter.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 29/06/21.
//

import Foundation

public protocol CharactersPresenterProtocol {
    func getCharacters(completion: @escaping (([Character]) -> Void))
}

class CharactersPresenter: CharactersPresenterProtocol {
    
    struct Dependencies {
        var netWorkManager: NetworkManager = NetworkManager()
    }
    
    var dependencies: Dependencies
    
    init(dependencies: Dependencies = .init()) {
        self.dependencies = dependencies
    }
    
    public func getCharacters(completion: @escaping (([Character]) -> Void)) {
        var characters: [Character] = []
        self.dependencies.netWorkManager.getCharacters { (responseJson) in
            guard let response = responseJson else {
                completion([])
                return
            }
            
            characters = response.data.results
            completion(characters)
        }
    }
}
