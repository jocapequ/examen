//
//  Network.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 29/06/21.
//

import Foundation

struct Endpoints {
    static var characters: String = "https://gateway.marvel.com:443/v1/public/characters"
}

struct Constants {
    static let privateKey = "3962f1fc3df6af3f2ca242b57a2366a764103929"
    static let publicKey = "2b235c15dc594ba1bd5942f241f8ad47"
}

class NetworkManager {
    
    public func getCharacters(completion: @escaping ((ResponseJson?) -> Void))  {
        let timestamp = Int(Date().timeIntervalSinceReferenceDate)
        let hash = ("\(timestamp)"+Constants.privateKey+Constants.publicKey).MD5
        let endpoint = "\(Endpoints.characters)?limit=100&ts=\(timestamp)&apikey=\(Constants.publicKey)&hash=\(hash)"
        //print(endpoint)
        
        guard let url = URL(string: endpoint) else {
            print("Error: cannot create URL")
            return
        }
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
                
            guard let responseJson = try? JSONDecoder().decode(ResponseJson.self, from: data) else {
                completion(nil)
                return
            }
            completion(responseJson)
            print(response)
           
        }.resume()
    }
}
