//
//  Character.swift
//  ExamenSrPago
//
//  Created by Macbook Pro on 29/06/21.
//

import Foundation

public struct ResponseJson: Decodable {
    var data: DataJson
}

public struct DataJson: Decodable {
    var results: [Character]
}

public struct Character: Decodable {
    var id: Int
    var name: String
    var description: String
    var thumbnail: Thumbnail
}

public struct Thumbnail: Decodable {
    enum CodingKeys: String, CodingKey {
            case path
            case extensionString = "extension"
    }
    var path: String
    var extensionString: String
}
